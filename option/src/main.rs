enum Gender {
    Male,
    Female,
}

struct Person {
    name: String,
    age: u32,
    gender: Option<Gender>,
}

impl Gender {
    fn get_pronoun(&self) -> (String, String) {
        match self {
            Gender::Male => (String::from("He"), String::from("His")),
            Gender::Female => (String::from("She"), String::from("Her")),
        }
    }
}

impl Person {
    fn pretty_print(&self) {
        let mut pronoun = (String::from("Xe"), String::from("Xyr"));

        if let Some(gender) = &self.gender {
            pronoun = gender.get_pronoun();
        }

        let (p_sub, p_obj) = pronoun;

        println!(
            "{p_obj} name is {}. {p_sub} is {} years old.",
            self.name, self.age
        );
    }
}

fn main() {
    let p1 = Person {
        name: String::from("Faiz Jazadi"),
        age: 19,
        gender: Some(Gender::Male),
    };

    let p2 = Person {
        name: String::from("Rachelia Arnoldi"),
        gender: Some(Gender::Female),
        ..p1
    };

    let p3 = Person {
        name: String::from("Jane John"),
        gender: None,
        ..p2
    };

    p1.pretty_print();
    p2.pretty_print();
    p3.pretty_print();
}
