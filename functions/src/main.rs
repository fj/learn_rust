fn main() {
    println!("Hello, world!");
    another_function();
    another_function_wparam(0x3000);
    println!("another_function_wretval(10) returns {:?}", another_function_wretval(10));
}

fn another_function() {
    println!("Another function hehe");
}

fn another_function_wparam(x: i32) {
    println!("The value of x is {x:?}");
}

fn another_function_wretval(x: i32) -> i32 {
    x + x
}
