use std::io;

fn main() {
    let mut inp = String::new();

    // borrow a mutable reference of `inp`
    io::stdin().read_line(&mut inp).unwrap();
    // borrowed item returned

    inp = String::from(inp.trim());
    // borrow an immutable string slice from inp
    println!("The first word is: {:?}", first_word(&inp[..]));
}


// accepts a string slice, returns a string slice
fn first_word(sentence: &str) -> &str {
    let bytes: &[u8] = sentence.as_bytes();

    for (i, &c) in bytes.iter().enumerate() {
        if c == b' ' {
            return &sentence[0..i];
        }
    }

    &sentence[..]
}
