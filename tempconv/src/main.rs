use std::io;
use std::io::Write;

fn main() {
    loop {
        println!("(1) c to f; (2) f to c; (3) exit");
        print!("> your choice: ");
        io::stdout().flush().expect("Failed to flush");

        let mut choice = String::new();
        io::stdin()
            .read_line(&mut choice)
            .expect("Failed to read input");

        let choice: i32 = choice.trim().parse().expect("Failed to parse input");

        if choice == 3 {
            break;
        } else if choice > 3 || choice < 1 {
            continue;
        }

        print!("> A = ");
        io::stdout().flush().expect("Failed to flush");

        let mut inp_temp = String::new();
        io::stdin()
            .read_line(&mut inp_temp)
            .expect("Failed to read input");

        let inp_temp: f64 = inp_temp.trim().parse().expect("Failed to parse input");

        let result: f64 = match choice {
            1 => 1.8 * inp_temp + 32.0,
            2 => (inp_temp - 32.0) / 1.8,
            _ => break,
        };

        println!("> B = {result}");
    }
}
