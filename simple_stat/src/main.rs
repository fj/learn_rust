use std::io;
use std::collections::HashMap;

fn sum(data: &Vec<i32>) -> i32 {
    data.iter().sum()
}

fn mean(data: &Vec<i32>) -> f32 {
    data.iter().sum::<i32>() as f32 / data.len() as f32
}

fn mode(data: &Vec<i32>) -> i32 {
    let mut map = HashMap::new();
    for record in data {
        let count = map.entry(record).or_insert(0);
        *count += 1;
    }
    **map.iter().max_by_key(|&(_, v)| v).unwrap().0
}

fn median(data: &Vec<i32>) -> f32 {
    let mid = data.len() / 2;
    if data.len() % 2 == 0 {
        return (data[mid - 1] + data[mid]) as f32 / 2.0
    }
    data[mid] as f32
}

fn main() {
    println!("Simple Statistics Calculator");

    let mut raw_data = String::new();
    let mut data: Vec<i32> = Vec::new();
    println!("Insert your data (separated by spaces):");

    io::stdin()
        .read_line(&mut raw_data)
        .expect("Failed to read line");

    for (i, word) in raw_data.split_whitespace().enumerate() {
        let word: i32 = match word.parse() {
            Ok(num) => num,
            Err(e) => {
                println!(
                    "Unable to parse data ({}) at position {}, ignoring...",
                    e, i
                );
                continue;
            }
        };
        data.push(word);
    }

    data.sort();
    println!("Sorted data: {:?}", data);

    println!("Sum: {}", sum(&mut data));
    println!("Mean: {}", mean(&mut data));
    println!("Mode: {}", mode(&mut data));
    println!("Median: {}", median(&mut data));
}
