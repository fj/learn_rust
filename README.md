# learn_rust
This repository contains my progression through The Rust Programming Language book.

The book is free, accessible [here](https://doc.rust-lang.org/book/).
