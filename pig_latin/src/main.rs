use std::io;

fn main() {
    println!("Enter your sentence below:");
    let mut sentence = String::new();
    io::stdin()
        .read_line(&mut sentence)
        .expect("Invalid string");

    sentence = sentence.to_lowercase();
    let words: Vec<&str> = sentence.split_whitespace().collect();
    let pig_words: Vec<String> = words.iter().map(|s| convert_to_pig_word(s)).collect();
    println!("{}", pig_words.join(" "));
}

fn convert_to_pig_word(word: &str) -> String {
    let first_char = word.chars().next().unwrap();
    let mut pig_word = String::new();
    let mut suffix_first_char = 'h';

    match first_char {
        'a' | 'i' | 'u' | 'e' | 'o' => pig_word.push_str(word),
        _ => {
            pig_word.push_str(&word[1..]);
            suffix_first_char = first_char;
        }
    }

    pig_word.push('-');
    pig_word.push(suffix_first_char);
    pig_word += "ay";

    return pig_word;
}
