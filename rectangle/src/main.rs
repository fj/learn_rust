struct Rectangle {
    width: u32,
    height: u32,
}

impl Rectangle {
    fn can_hold(&self, another_rect: &Rectangle) -> bool {
        if self.width > another_rect.width && self.height > another_rect.height {
            return true;
        }
        false
    }

    fn square(size: u32) -> Rectangle {
        Rectangle {
            width: size,
            height: size,
        }
    }
}

fn main() {
    let r1 = Rectangle {
        width: 30,
        height: 50,
    };

    let r2 = Rectangle {
        width: 10,
        height: 40,
    };

    let r3 = Rectangle {
        width: 60,
        height: 45,
    };

    let sq = Rectangle::square(70);

    println!("Can r1 hold r2? {}", r1.can_hold(&r2));
    println!("Can r1 hold r3? {}", r1.can_hold(&r3));
    println!("Can sq hold r1? {}", sq.can_hold(&r1));
}
