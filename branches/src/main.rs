fn main() {
    let number = 0;

    if number < 10 {
        println!("If condition met, value of number is {number:?}");
    } else if number == 0 {
        println!("If condition met, value of number is zero");
    } else {
        println!("If condition not met, value of number is {number:?}");
    }

    print_amodb(number, number-10);
}

fn print_amodb(a: i32, b: i32) {
    let amodb = if b == 0 {
        0
    } else {
        a % b
    };
    println!("A mod B is {amodb}");
}
