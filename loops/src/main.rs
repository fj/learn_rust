fn main() {
    let mut counter = 0;

    let x = loop {
        if counter > 10 {
            break counter;
        }

        println!("Again!!!");
        counter += 1;
    };

    println!("The end value of x is {x:?}");

    println!("Now let's do a countdown before launch");

    print_countdown();

    iterate_arr_manual();

    print_countdown_with_range();
}

fn iterate_arr_manual() {
    // this function uses a `while` loop to iterate manually through an array

    let arr = [1, 3, 5, 7, 9, 11];
    let mut index = 0;

    while index < 6 {
        println!("arr[{index}] -> {}", arr[index]);
        index += 1;
    }

    // safer approach is by looping with an iterator

    for el in arr.iter() {
        println!("Current iteration: {el}");
    }
}

fn print_countdown() {
    let mut counter = 3;
    print!("Launching in ");
    while counter != 0 {
        print!("{counter}.. ");
        counter -= 1;
    }
    println!("LIFTOFF!!!");
}

fn print_countdown_with_range() {
    for number in (1..4).rev() {
        print!("{number}.. ");
    }
    println!("LIFTOFF!!!");
}
