use std::io;

#[derive(Debug)]
struct Person {
    name: String,
    age: u32,
}

fn main() {
    println!("What is your name?");
    let mut name = String::new();
    io::stdin().read_line(&mut name).unwrap();
    name = String::from(name.trim());

    println!("How old are you? ");
    let mut age = String::new();
    io::stdin().read_line(&mut age).unwrap();
    let age: u32 = age.trim().parse().unwrap();

    let p = Person {
        name: String::from(name),
        age
    };

    print_person(&p);
    println!("Debug information for `p``: {:?}", &p);
}

fn print_person(p: &Person) {
    println!("The person's name is {} and their age is {}", p.name, p.age);
}
