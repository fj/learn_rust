fn main() {
    let mut v: Vec<i32> = Vec::new();
    v.push(1);
    v.push(2);
    v.push(3);
    let third: &i32 = &v[2];
    match v.get(1) {
        Some(second) => println!("The second element is {}", second),
        None => println!("There is no second element!"),
    }
    println!("The third element is {}", third);

    let mut v2 = vec![100, 50, 25];
    for i in &mut v2 {
        *i += 10;
        println!("{}", &i);
    }
}
