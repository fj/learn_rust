use std::io;

fn main() {
    let mut n = String::new();
    io::stdin().read_line(&mut n).unwrap();
    let n: u32 = n.trim().parse().expect("Failed to parse value");

    for y in 0..n {
        println!("fibo({y}) = {}", fibonacci_n(y));
    }
}

fn fibonacci_n(n: u32) -> u32 {
    match n {
        0 => 1,
        1 => 1,
        _ => fibonacci_n(n - 2) + fibonacci_n(n - 1),
    }
}
