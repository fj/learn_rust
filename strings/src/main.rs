use std::io;

fn main() {
    // Show that a `String` type can be mutated
    let mut s = String::from("Hello, ");

    io::stdin().read_line(&mut s).unwrap();
    s = String::from(s.trim());

    println!("{}!", s);
}
