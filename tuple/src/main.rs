fn main() {
    println!("Hello, world!");
    let tup: (i32, f64, bool) = (64, 3.1415, false);
    let another_tup = (false, 3.1, 20);

    println!("tup={tup:?}, another_tup={another_tup:?}")
}

