use std::collections::HashMap;
use std::io;

fn main() {
    let mut sentence = String::new();
    let mut map = HashMap::new();

    println!("Insert your sentence:");
    io::stdin()
        .read_line(&mut sentence)
        .expect("Failed to read line");

    for word in sentence.split_whitespace() {
        let count = map.entry(word.to_lowercase()).or_insert(0);
        *count += 1;
    }

    for (word, count) in map {
        println!("Word {} appeared {} times", word, count);
    }
}
